import java.util.Scanner;

public class OXgames {
static char table [][]= new char [3][3] ; 
static Scanner kb = new Scanner(System.in);
public static void showTable(){
	for(int i = 0 ;i < 3;i ++){
        for(int j = 0 ;j < 3;j ++){
        	if(table[i][j]==0) {
        		System.out.print("-");
        	}else {
        		System.out.print(table[i][j]);
        	}
           System.out.print(" ");  
        }
        System.out.println("");
	  }
	}
	static boolean checkWinRow(char player,int row) {
		return table[row][0] == player
			&& table[row][1] == player	
			&& table[row][2] == player	

	}
	static boolean checkWinCol(char player,int col) {
		return table[0][col] == player
			&& table[1][col] == player	
			&& table[2][col] == player	
	}
	static boolean checkWinCross1(char player){   	
    	return table[0][0]== player 
    		&&	table[1][1]== player
    		&& 	table[2][2]== player;  		
    }
	static boolean checkWinCross2(char player){   	
    	return table[0][2]== player 
	    	&&	table[1][1]== player
	    	&& 	table[2][0]== player;  		
	}
	public static void getInput(char player){
   	 System.out.print("Please input row,col: ");
   	 int choice1 = kb.nextInt()-1;
		 int choice2 = kb.nextInt()-1; 
		 checkInput(choice1,choice2,player);
   }
	static void addInput(int row,int col,char player){
		table [row][col] = player;
		checkWin(row,col,player);
	}
	static void checkInput(int i,int o,char player) {
    	try{
		    if(table [i][o] == 0) {
		    	addInput(i,o,player) ;
		    }else {
		    	System.out.println("row , col is not empty.");
		    	getInput(player);	
	    	}
    	}catch(ArrayIndexOutOfBoundsException exception) {
    		System.out.println("row , col is out of range.");
	    	getInput(player);
    	}
    }
	static char switchTurn(char player) {
		if(player=='O') {
        	return 'X' ;
        }else {
        	return 'O';
        }
    }
	static void checkDraw(){
        int draw = 0;
        for(int i = 0 ;i < 3;i ++){
            for(int j = 0 ;j < 3;j ++){
				if(table[i][j]==0) {              
				}else {
		                   draw = draw+1; 
				}
            }
        }
        if(draw == 9){
        	showTable();
        	System.out.println("-----------------------");
            System.out.println("Draw!!");
            System.exit(1); 
        } 
    } 
	static void checkWin(int row,int col,char player) {
    	if(checkWinRow(player,row)||checkWinCol(player,col)||
    			checkWinCross1(player)||checkWinCross2(player)) {
    		showTable();
    		System.out.println("-----------------------");
    		System.out.print(player+" WIN");
    		System.exit(1);
    	}else {
    		checkDraw();
    	}
    }
	
    public static void main(String[]ags){
    	char player = 'O' ;
    	System.out.println("Welcome to OX game!!!");
    	for(;;){
	        showTable();
	        System.out.println("Turn "+player);
	        getInput(player);
	        player = switchTurn(player);
    }
}